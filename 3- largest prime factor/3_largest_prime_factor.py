from math import sqrt
import time


def main():
    start = time.time()
    a = biggest_factor(60085147514311111133)
    if a is not None:
        print("ergebnis: ", a)
    else:
        print("ist Primzahl!")

    stop = time.time()
    print(stop-start, " seconds needed!")

def biggest_factor(number):
    number = int(number)
    upper_limit = int(sqrt(number) + 1)
    if number % 2 == 0:
        possible_prime = int(number / 2)
        if is_prime(possible_prime):
            return possible_prime

    # will return largest prime if it is above sqrt(number)
    for i in range(3, upper_limit, 2):
        if number % i == 0:
            possible_prime = number / i
            if is_prime(possible_prime):
                return int(possible_prime)

    # will return largest prime if it is below sqrt(number)
    biggest_prime = 0
    if number % 2 == 0:
        biggest_prime = 2

    for i in range(3, upper_limit, 2):
        if number % i == 0:
            if is_prime(i):
                biggest_prime = i
    if biggest_prime > 0:
        return int(biggest_prime)
    else:
        return None

# check if given number is
def is_prime(number):
    upper_limit = int(sqrt(number) + 1)
    if number % 2 == 0:
        return False
    for i in range(3, upper_limit, 2):
        if number % i == 0:
            return False
    return True


if __name__ == "__main__":
    main()
