'''
    Implementatin using a List
    --> little less fast than array
    --> more memory than array needed, propably because of storing big numbers
'''
import time


def main():
    range = 100000000

    start_time = time.time()
    amount = sum_of_multiples(range)

    duration = time.time() - start_time

    print(amount)
    print("Time needed to calculate sum of multiples up to ", range, ": ", duration, " seconds!")


def sum_of_multiples(size):
    array = [0]
    for i in range(0, size + 1, 3):
        array.append(i)
    for j in range(0, size + 1, 5):
        array.append(j)

    for k in range(0, size + 1, 15):
        array.append((-1) * k)

    sum_multiples = 0
    for l in range(0, len(array)):
        sum_multiples += array[l]

    return sum_multiples


if __name__ == "__main__":
    main()
