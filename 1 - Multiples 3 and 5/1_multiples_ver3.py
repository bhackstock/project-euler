'''
    Implementation only single loop
    --> less memory consumption
'''
import time


def main():
    range = 10000000

    start_time = time.time()
    amount = sum_of_multiples(range)

    duration = time.time() - start_time

    print(amount)
    print("Time needed to calculate sum of multiples up to ", range, ": ", duration, " seconds!")


def sum_of_multiples(size):
    sum_multiples = 0

    for i in range(0, size + 1):
        if i % 3 == 0 or i % 5 == 0:
            sum_multiples += i

    return sum_multiples


if __name__ == "__main__":
    main()
