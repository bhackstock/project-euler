'''
    Implementatin using an "array"
    --> very Memory intensive
    --> very fast
'''
import time


def main():
    range = 1000000000

    start_time = time.time()
    amount = sum_of_multiples(range)

    duration = time.time() - start_time

    print(amount)
    print("Time needed to calculate sum of multiples up to ", range, ": ", duration, " seconds!")


def sum_of_multiples(size):
    array = [None] * (size + 1)
    for i in range(0, size + 1, 3):
        array[i] = 1
    for j in range(0, size + 1, 5):
        array[j] = 1

    sum_multiples = 0
    for k in range(0, size + 1):
        if array[k] is not None:
            sum_multiples += k

    return sum_multiples


if __name__ == "__main__":
    main()
