def main():
    print(fib_sum(4000000))


def fib_sum(limit):
    if limit == 0:
        return 0
    elif limit == 1:
        return 1
    elif limit <= 3:
        return 3

    summe = 0
    a, b, i = 1, 2, 0
    while i < limit:
        if i % 2 == 0:
            summe += b
        a, b = get_next_fib(a, b)
        i = b

    return summe


def get_next_fib(a, b):
    if a > b:
        return a, a + b
    return b, a + b


if __name__ == "__main__":
    main()
