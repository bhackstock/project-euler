import time
from math import sqrt


def primfacs(n):
    i = 2
    primfac = []
    while i * i <= n:
        while n % i == 0:
            primfac.append(i)
            n = n / i
        i = i + 1
    if n > 1:
        primfac.append(n)
    return primfac


number = 333311
print("Größter faktor von, ", number, "(sqrt = ", sqrt(number), ")")

start = time.time()
primfacs = primfacs(number)

print(primfacs)

if int(max(primfacs)) > int(sqrt(number)):
    print("Bigger than root")

print(time.time() - start, " seconds needed!")
