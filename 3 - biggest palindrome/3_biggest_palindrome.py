import time


def main():
    start = time.time()
    biggest_palindrom = calculate_biggest_palindrome(6)
    stop = time.time()
    print(biggest_palindrom, " Dauer: ", stop - start)

    # print(is_palindrom(900099, 6))


def calculate_biggest_palindrome(stellen):
    upper_limit = 10 ** stellen - 1
    lower_limit = 10 ** (stellen - 1)
    max = 0
    i_max = 0
    j_max = 0
    for i in range(upper_limit, lower_limit, -1):
        for j in range(upper_limit, i, -1):
            if i < i_max and j < j_max:
                break

            possible_palindrom = i * j
            if possible_palindrom < max:
                break

            laenge = calc_stellen(possible_palindrom)

            if is_palindrom(possible_palindrom, laenge):
                print(i, " und ", j)
                if possible_palindrom > max:
                    max = possible_palindrom
                    i_max = i
                    j_max = j
    return max


def is_palindrom(zahl, laenge):
    if laenge <= 1:
        return True

    stellen = calc_stellen(zahl)

    links_null = False
    ziffer_links = 0

    if stellen < laenge:
        links_null = True

    ziffer_rechts = zahl % 10
    temp_zahl = zahl

    if not links_null:
        while temp_zahl > 0:
            ziffer_links = temp_zahl
            temp_zahl = int(temp_zahl / 10)

    if ziffer_links == ziffer_rechts:
        zahl = int(zahl / 10)  # rechteste stelle loswerden
        zahl = zahl - ziffer_links * (10 ** (stellen - 2))

        return is_palindrom(zahl, laenge - 2)

    else:
        return False


def calc_stellen(zahl):
    stellen = 0
    while zahl > 0:
        zahl = int(zahl / 10)
        stellen = stellen + 1
    return stellen


if __name__ == "__main__":
    main()
